;
; ProvaPiezoAsm.asm
;
; Created: 27/01/2018 00:23:17
; Author : Poldo
;

#include "tn85def.inc"

.org 0x0000
	rjmp INIT
.org 0x0004
	rjmp TIMER1_OVF

INIT:
	sbi DDRB,DDB0 ;set pin b0 output direction
	sbi DDRB,DDB1 ;set pin b1 output direction
	sbi DDRB,DDB2 ;set pin b2 output direction
	;INIT_TIMER0_CTC_MODE: ;configures CTC mode to outputs notes frequency through OC0A (PB0)
	in r16,TCCR0A
	sbr r16,(1<<COM0A0)+(1<<WGM01)
	out TCCR0A,r16
	;INIT_TIMER1: ;used in normal counter mode to count notes duration, IMPORTANT: r0 needed to extend the counter
	in r16,TIMSK
	sbr r16,(1<<TOIE1) ;overflow interrupt enable
	out TIMSK,r16
	;LOAD_FIRST_NOTE_AND_ACTIVATE_TIMERS:
	ldi zh,high(SONG*2) ;z points to the first note
	ldi zl,low(SONG*2)
	lpm r16,z+
	;load timer0 with the first note frequency
	out OCR0A,r16
	;load timer1 with the first note duration
	lpm r0,z+
	;activate timer0
	in r16,TCCR0B
	sbr r16,(1<<CS01)+(1<<CS00) ;set Timer0 Clock Signal to ck/64 
	out TCCR0B,r16
	;activate timer1
	in r16,TCCR1
	sbr r16,(1<<CS13)+(1<<CS12)+(1<<CS11) ;set Timer1 Clock Signal to ck/8192
	out TCCR1,r16
	;configure leds
	sbi PORTB,PORTB1
	sei ;(bset SREG_I) global interrupt enable

START:
	rjmp START

TIMER1_OVF:
	dec r0 ;decrement the duration register
	brne END ;if duration is not over, continue to play the current note
UPDATE_NOTE: ;else update the note
	push r17
	push r18
	in r18,SREG ;save status and registers 
	in r17,TCCR0B
	cbr r17,(1<<CS01)+(1<<CS00) ;temporarily stop timer0 to change its frequence
	out TCCR0B,r17 
	cbi PORTB,PORTB0 ;clear the output to stop playing the old note
	ldi r17,0 ;reset timer0
	out TCNT0,r17
	lpm r17,z+ ;read the next frequency
	;check if the new note is a pause (0) or the last note (255)
	;if not pause -> jmp to check if it's the last note - else PAUSE output
	;if not end of notes -> jmp ahead - else reload first note
CHECK_PAUSE:
	cpi r17,0
	brne CHECK_LAST_NOTE
	lpm r0,z+ ;load the duration register with the pause duration
	rjmp END_UPDATE
CHECK_LAST_NOTE:
	cpi r17,0xFF
	brne LOAD_FREQUENCY_AND_DURATION
	ldi zh,high(SONG*2)
	ldi zl,low(SONG*2)
	lpm r17,z+ ;reload the first note
LOAD_FREQUENCY_AND_DURATION:
	out OCR0A,r17
	lpm r0,z+ 
	;RESTART_TIMER:
	in r17,TCCR0B
	sbr r17,(1<<CS01)+(1<<CS00) ;restart the timer
	out TCCR0B,r17 
	sbi PINB,PINB1
	sbi PINB,PINB2
END_UPDATE:
	out SREG,r18
	pop r18
	pop r17
END:
	reti

SONG: .db 238,2,238,2,212,4,238,4,\
		  178,4,188,8,238,2,238,2,\
		  212,4,238,4,158,4,178,8,\
		  238,2,119,4,141,4,178,4,\
		  188,4,212,12,133,2,133,2,\
		  141,4,178,4,158,4,178,8,\
		  0,8,\
		  0xFF